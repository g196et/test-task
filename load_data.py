"""
Файлик с методами для парсинга данных и записи в БД.
"""
import os
import sys
import requests
import datetime

from bs4 import BeautifulSoup
import psycopg2
from multiprocessing.dummy import Pool as ThreadPool


DB_CONNECT_KEYWORDS = {
    'user': os.environ['DB_USERNAME'],
    'password': os.environ['DB_PASSWORD'],
    'dbname': os.environ['DB_NAME'],
    'host': os.environ['DB_HOST'],
    'port': os.environ['DB_PORT']
}


def __load_data(short_name):
    """Служебный метод для полной загрузки данных.

    Args:
        short_name (str): Короткое название компании.
    """
    conn = psycopg2.connect(**DB_CONNECT_KEYWORDS)
    conn.autocommit = True
    with conn.cursor() as cursor:
        # Запрос для upsert'а компании
        sql_upsert_company = """
            INSERT INTO stocks_companymodel (short_name)
            VALUES (%s::TEXT)
            ON CONFLICT (short_name)
            DO NOTHING
            RETURNING id"""
        cursor.execute(sql_upsert_company, (short_name, ))
        company_id = cursor.fetchone()[0]
    conn.close()

    __parse_history_data(short_name, company_id)
    __parse_insider_trades(short_name, company_id)


def __parse_history_data(short_name, company_id):
    """Парсинг данных с историей цен.

    Args:
        short_name (str): Короткое название компании.
        company_id (int): Идентификатор компании в нашей локальной базе.
    """
    # Парсим данные истории
    url = f'https://www.nasdaq.com/symbol/{short_name}/historical'
    history_request = requests.get(url)
    soup = BeautifulSoup(history_request.text, 'lxml')
    rows = soup.find('div', {'class': 'genTable'}).find('div').find('table').find('tbody').find_all('tr')
    for index, row in enumerate(rows):
        # Какая-то дичь возвращается в первом элементе! TODO: перепроверить!
        if index == 0:
            continue
        columns = row.find_all('td')
        date_data = [int(element.strip()) for element in columns[0].text.split('/')]
        date = datetime.date(date_data[2], date_data[0], date_data[1])
        open_price = columns[1].text.strip()
        high_price = columns[2].text.strip()
        low_price = columns[3].text.strip()
        close_price = columns[4].text.strip()
        volume = columns[5].text.strip().replace(',', '')
        conn = psycopg2.connect(**DB_CONNECT_KEYWORDS)
        conn.autocommit = True
        with conn.cursor() as cursor:
            # Запрос для upsert'а компании
            sql_upsert_history = """
                INSERT INTO stocks_companyhistorymodel
                (open_price, close_price, max_price, min_price, volume, date, company_id)
                VALUES (%s::FLOAT, %s::FLOAT, %s::FLOAT, %s::FLOAT, %s::INTEGER, %s::DATE, %s::INTEGER)
                ON CONFLICT (date, company_id)
                DO NOTHING"""
            params = (open_price, close_price, high_price, low_price, volume, date, company_id)
            cursor.execute(sql_upsert_history, params)
        conn.close()


def __parse_insider_trades(short_name, company_id):
    """Парсинг данных сделок.

    Args:
        short_name (str): Короткое название компании.
        company_id (int): Идентификатор компании в нашей локальной базе.
    """
    # Парсим данные сделок
    for page_number in range(1, 11, 1):
        row_count = 0
        url = f'https://www.nasdaq.com/symbol/{short_name}/insider-trades?page={page_number}'
        trade_quest = requests.get(url)
        soup = BeautifulSoup(trade_quest.text, 'lxml')
        table = soup.find('div', {'class': 'genTable'}).find('table')
        if table.find('tbody'):
            table = table.find('tbody')
        rows = table.find_all('tr')
        for index_row, row in enumerate(rows):
            if index_row == 0:
                continue
            columns = row.find_all('td')
            trader_name = columns[0].find('a').text.strip()
            relation = columns[1].text.strip()
            last_date_data = [int(element.strip()) for element in columns[2].text.split('/')]
            last_date = datetime.date(last_date_data[2], last_date_data[0], last_date_data[1])
            transaction_type = columns[3].text.strip()
            owner_type = columns[4].text.strip()
            shares_trade = columns[5].text.strip().replace(',', '')
            last_price = columns[6].text.strip() or None
            shares_held = columns[7].text.strip().replace(',', '')
            conn = psycopg2.connect(**DB_CONNECT_KEYWORDS)
            conn.autocommit = True
            with conn.cursor() as cursor:
                # Запрос для upsert'а компании
                sql_upsert_history = """
                    INSERT INTO stocks_insidertrademodel
                    (name, relation, last_date, transaction_type, owner_type, shares_traded, last_price, shares_held,
                     company_id)
                    VALUES (%s::TEXT, %s::TEXT, %s::DATE, %s::TEXT, %s::TEXT, %s::INT, %s::FLOAT, %s::FLOAT, %s::INT)"""
                params = (
                    trader_name,
                    relation,
                    last_date,
                    transaction_type,
                    owner_type,
                    shares_trade,
                    last_price,
                    shares_held,
                    company_id
                )
                cursor.execute(sql_upsert_history, params)
            conn.close()
            row_count += 1
        #  Если меньше 15, значит это последняя страница и дальше информация начнет дублироваться.
        if row_count < 15:
            break


def run(thread_count):
    """Метод для запуска задачи на парсинг данных и заполнения БД.

    Args:
        thread_count (int): Кол-во потоков в которых будет выполняться задача.
    """
    with open('tickers.txt', 'r') as file:
        short_names = [line.replace('\n', '').lower() for line in file]

        pool = ThreadPool(thread_count)
        pool.map(__load_data, short_names)

        # close the pool and wait for the work to finish
        pool.close()
        pool.join()


if __name__ == "__main__":
    try:
        run(int(sys.argv[1]))
    except (ValueError, TypeError) as e:
        raise Exception(str(e))
