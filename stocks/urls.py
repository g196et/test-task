"""Урлы для сервиса stocks"""

from django.urls import path

from . import views


urlpatterns = [
    # ex: /
    path('', views.index, name='index'),
    # API URLS
    path('api/', views.api_index, name='api_index'),
    path('api/<str:ticker>/', views.api_prices, name='api_prices'),
    path('api/<str:ticker>/insider', views.api_insiders_list, name='api_insiders_list'),
    path('api/<str:ticker>/insider/<str:name>', views.api_insider_data, name='api_insider_data'),
    # ex: /api/aapl/analytics?date_from=14/05/2019&date_to=30/05/2019
    path('api/<str:ticker>/analytics', views.api_analytics, name='api_analytics'),
    # ex: /api/goog/delta?value=23&type=open
    path('api/<str:ticker>/delta', views.api_delta, name='api_delta'),
    # ex: /aapl/
    path('<str:ticker>/', views.prices, name='prices'),
    # ex: /aapl/insider
    path('<str:ticker>/insider', views.insiders_list, name='insiders_lists'),
    # ex: /aapl/insider/konstantin_malakhov
    path('<str:ticker>/insider/<str:name>', views.insider_data, name='insider_data'),
    # ex: /aapl/analytics
    path('<str:ticker>/analytics', views.analytics, name='analytics'),
    # ex: /aapl/delta
    path('<str:ticker>/delta', views.delta, name='delta'),
]
