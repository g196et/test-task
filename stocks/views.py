from datetime import datetime

from django.shortcuts import render
from django.db import connection
from django.http import JsonResponse

from .models import CompanyModel, CompanyHistoryModel, InsiderTradeModel


def __get_index_data():
    """Получить список компаний"""
    return CompanyModel.objects.all()


def __get_price_list_data(ticker):
    """Получить список цен компании"""
    return CompanyHistoryModel.objects.filter(company__short_name=ticker).order_by('-date')


def __get_insiders_list_data(ticker):
    """Получить список сделок компании"""
    return InsiderTradeModel.objects.filter(company__short_name=ticker).order_by('-last_date', 'name')


def __get_insider_data(ticker, name):
    """Получить список сделок определенного лица"""
    result = InsiderTradeModel.objects.filter(company__short_name=ticker)
    result = result.filter(name=name.upper()).order_by('-last_date', 'name')
    return result


def __get_analytics_data(str_date_from, str_date_to, ticker):
    """Получить данные для анализа цен"""
    if not str_date_from or not str_date_to:
        return []
    data_from = datetime.strptime(str_date_from, '%d/%m/%Y').date()
    data_to = datetime.strptime(str_date_to, '%d/%m/%Y').date()
    data = CompanyHistoryModel.objects.filter(company__short_name=ticker, date__lte=data_to, date__gte=data_from)
    data = data.order_by('-date')
    result = []
    prev_data = None
    for item in data.all():
        if not prev_data:
            prev_data = item
            continue
        result.append({
            'date_range': f'{prev_data.date} - {item.date}',
            'open_price': {
                'prev_data': prev_data.open_price,
                'next_data': item.open_price,
                'diff': round(item.open_price - prev_data.open_price, 3),
            },
            'close_price': {
                'prev_data': prev_data.close_price,
                'next_data': item.close_price,
                'diff': round(item.close_price - prev_data.close_price, 3),
            },
            'max_price': {
                'prev_data': prev_data.max_price,
                'next_data': item.max_price,
                'diff': round(item.max_price - prev_data.max_price, 3)
            },
            'min_price': {
                'prev_data': prev_data.min_price,
                'next_data': item.min_price,
                'diff': round(item.min_price - prev_data.min_price, 3)
            },
        })
        prev_data = item

    return result


def __get_delta_data(value, price_type, ticker):
    """Получить данные самых коротких периодов за которые произошло изменение цены"""
    if not value or not price_type:
        return []
    transfer_dict = {
        'open': 'open_price',
        'close': 'close_price',
        'high': 'max_price',
        'low': 'min_price',
    }
    field_name = transfer_dict.get(price_type)
    sql_params_dict = {
        'value': value,
        'ticker': ticker,
    }
    sql_query_text = f"""
        WITH cte AS (
            SELECT h.*
            FROM stocks_companyhistorymodel AS h
            JOIN stocks_companymodel AS c
                ON h.company_id = c.id
            WHERE c.short_name = %(ticker)s
            ORDER BY h."date"
        )
        SELECT
            ARRAY_AGG(cte1.date) AS from_date,
            ARRAY_AGG(cte2.date) AS to_date,
            MIN(cte2.date - cte1.date) AS custom_min
        FROM
            cte AS cte1,
            cte AS cte2
        WHERE
            cte1.date < cte2.date
            AND abs(cte2.{field_name} - cte1.{field_name}) >= %(value)s::INTEGER
        GROUP BY (cte2.date - cte1.date)
        HAVING MIN(cte2.date - cte1.date) = (cte2.date - cte1.date)
        ORDER BY custom_min
        LIMIT 1
        """
    with connection.cursor() as cursor:
        cursor.execute(sql_query_text, sql_params_dict)
        result = cursor.fetchone()
        new_result = []
        if result:
            for date_from, date_to in zip(result[0], result[1]):
                new_result.append((date_from, date_to))

    return new_result


def index(request):
    companies = __get_index_data()
    return render(request, 'stocks/index.html', {'companies': companies})


def prices(request, ticker):
    result = __get_price_list_data(ticker)
    return render(request, 'stocks/prices.html', {'prices': result})


def insiders_list(request, ticker):
    result = __get_insiders_list_data(ticker)
    return render(request, 'stocks/insider.html', {'insider_data': result})


def insider_data(request, ticker, name):
    result = __get_insider_data(ticker, name)
    return render(request, 'stocks/insider_data.html', {'insider_data': result})


def analytics(request, ticker):
    result = __get_analytics_data(
        request.GET.get('date_from'),
        request.GET.get('date_to'),
        ticker
    )
    return render(request, 'stocks/analytics.html', {'data': result})


def delta(request, ticker):
    result = __get_delta_data(
        request.GET.get('value'),
        request.GET.get('type'),
        ticker
    )
    return render(request, 'stocks/delta.html', {'data': result})


def api_index(request):
    companies = __get_index_data().values()
    return JsonResponse({'data': list(companies)})


def api_prices(request, ticker):
    result = __get_price_list_data(ticker).values()
    return JsonResponse({'data': list(result)})


def api_insiders_list(request, ticker):
    result = __get_insiders_list_data(ticker).values()
    return JsonResponse({'data': list(result)})


def api_insider_data(request, ticker, name):
    result = __get_insider_data(ticker, name).values()
    return JsonResponse({'data': list(result)})


def api_analytics(request, ticker):
    result = __get_analytics_data(
        request.GET.get('date_from'),
        request.GET.get('date_to'),
        ticker
    )
    return JsonResponse({'data': result})


def api_delta(request, ticker):
    result = __get_delta_data(
        request.GET.get('value'),
        request.GET.get('type'),
        ticker
    )
    return JsonResponse({'data': result})
