from django.db import models


class CompanyModel(models.Model):
    """Модель компании"""
    short_name = models.CharField(max_length=255, verbose_name='Короткое наименование', unique=True)

    def __str__(self):
        return self.short_name


class CompanyHistoryModel(models.Model):
    """Модель истории"""
    company = models.ForeignKey(to=CompanyModel, on_delete=models.CASCADE, verbose_name='Компания')
    date = models.DateField(verbose_name='Дата')
    open_price = models.FloatField(verbose_name='Цена открытия', null=True)
    close_price = models.FloatField(verbose_name='Цена закрытия', null=True)
    max_price = models.FloatField(verbose_name='Максимальная цена', null=True)
    min_price = models.FloatField(verbose_name='Минимальная цена', null=True)
    volume = models.FloatField(verbose_name='Объем', null=True)

    class Meta:
        unique_together = (('date', 'company'),)
        indexes = [
            models.Index(fields=['date', 'company']),
        ]


"""
По хорошему просится еще одна модель для отдельного человека и внешний ключ на него, но для выполнения задания
хватает и этого. Так же типы операций, владельца, должность можно сделать перечислением, но опять же в задании
не сказано об этом, и нет точных данных какие типы бывают, поэтому опустил этот момент
"""


class InsiderTradeModel(models.Model):
    """Модель сделки"""
    company = models.ForeignKey(to=CompanyModel, on_delete=models.CASCADE, verbose_name='Компания')
    name = models.CharField(max_length=255, verbose_name='Имя')
    relation = models.CharField(max_length=255, verbose_name='Должность', null=True)
    last_date = models.DateField(verbose_name='День', null=True)
    transaction_type = models.CharField(max_length=255, verbose_name='Тип операции', null=True)
    owner_type = models.CharField(max_length=255, verbose_name='Тип владельца', null=True)
    shares_traded = models.IntegerField(verbose_name='Акции', null=True)
    last_price = models.FloatField(verbose_name='Последняя цена', null=True)
    shares_held = models.FloatField(verbose_name='Принадлежащие акции', null=True)
